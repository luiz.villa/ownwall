EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sensors
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4750 2050 0    79   ~ 0
A/D CONVERTER AND DIGITAL ISOLATOR UNIT
Text Notes 4550 2400 0    59   ~ 0
This part 
Wire Notes Line
	4550 2150 6850 2150
Text HLabel 10250 1100 2    60   Output ~ 0
Vout
Wire Wire Line
	10250 1100 9850 1100
Text Label 9850 1100 0    60   ~ 0
Vout
Text Notes 10500 750  2    60   ~ 0
Output Variables
Wire Notes Line
	9550 1300 11000 1300
Wire Notes Line
	11000 1300 11000 1250
Text Notes 1750 750  0    60   ~ 0
Input Variables
Wire Notes Line
	4100 1850 4100 700 
Wire Notes Line
	750  1850 4100 1850
Text Label 3850 1650 2    60   ~ 0
GNDLow
Text Label 3850 1300 2    60   ~ 0
GNDPwR
Text Label 3850 1450 2    60   ~ 0
+5VLow
Text Label 3850 1100 2    60   ~ 0
+5VHigh
Text Label 1450 1600 2    60   ~ 0
VmT1+
Text Label 1450 1000 2    60   ~ 0
VmiL+
Wire Wire Line
	3450 1650 3850 1650
Text HLabel 3450 1650 0    60   Input ~ 0
GNDLow
Wire Wire Line
	3450 1300 3850 1300
Text HLabel 3450 1300 0    60   Input ~ 0
GNDPwR
Wire Wire Line
	3450 1450 3850 1450
Text HLabel 3450 1450 0    60   Input ~ 0
+5VLow
Wire Wire Line
	3450 1100 3850 1100
Text HLabel 3450 1100 0    60   Input ~ 0
+5VHigh
Wire Wire Line
	1050 1600 1450 1600
Wire Wire Line
	1050 1000 1450 1000
Text HLabel 1050 1600 0    60   Input ~ 0
VmT1+
Text HLabel 1050 1000 0    60   Input ~ 0
VmiL+
$Comp
L MCP3208 U?
U 1 1 596E2756
P 4700 4150
AR Path="/57067DAC/59547097/596E2756" Ref="U?"  Part="1" 
AR Path="/57067DAC/5955AA0A/596E2756" Ref="U?"  Part="1" 
AR Path="/57067DAC/5955B27F/596E2756" Ref="U?"  Part="1" 
AR Path="/596E2756" Ref="ADC"  Part="1" 
AR Path="/57067DAC/5975EFEF/596E2756" Ref="U?"  Part="1" 
F 0 "U?" H 4450 4650 50  0000 R CNN
F 1 "MCP3208" H 5350 3550 50  0000 R CNN
F 2 "" H 4800 4250 50  0001 C CNN
F 3 "" H 4800 4250 50  0001 C CNN
	1    4700 4150
	1    0    0    -1  
$EndComp
Wire Notes Line
	9550 550  9550 1300
$Comp
L ISO7341C Digitiso
U 1 1 596F6427
P 6550 4150
AR Path="/57067DAC/5955B27F/596F6427" Ref="Digitiso"  Part="1" 
AR Path="/57067DAC/5955AA0A/596F6427" Ref="Digitiso"  Part="1" 
AR Path="/57067DAC/5975EFEF/596F6427" Ref="Digitiso"  Part="1" 
F 0 "Digitiso" H 6300 4750 50  0000 C CNN
F 1 "ISO7341C" H 6250 3550 50  0000 C CNN
F 2 "SO-16-W" H 5650 3550 50  0001 C CIN
F 3 "" H 6550 4550 50  0000 C CNN
	1    6550 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5950 4250 6150 4250
Wire Wire Line
	5700 4550 6150 4550
Wire Wire Line
	5900 4350 6150 4350
Wire Wire Line
	5900 4250 5900 4350
Wire Wire Line
	5300 4250 5900 4250
Wire Wire Line
	6150 4450 5850 4450
Wire Wire Line
	5850 4450 5850 4350
Wire Wire Line
	5850 4350 5300 4350
Wire Wire Line
	5300 4150 5700 4150
Wire Wire Line
	5700 4150 5700 4550
Wire Notes Line
	6500 2900 6500 5200
Wire Notes Line
	6600 2900 6600 5200
Wire Wire Line
	4150 3300 6000 3300
Wire Wire Line
	4900 3650 4900 3300
Connection ~ 4900 3300
Wire Wire Line
	4600 3650 4600 3300
Connection ~ 4600 3300
Wire Wire Line
	4600 4750 4600 4950
Wire Wire Line
	4600 4950 4900 4950
Wire Wire Line
	4900 4950 4900 4750
$Comp
L Earth #PWR?
U 1 1 5970792A
P 4750 5000
AR Path="/57067DAC/5955B27F/5970792A" Ref="#PWR?"  Part="1" 
AR Path="/57067DAC/5955AA0A/5970792A" Ref="#PWR?"  Part="1" 
AR Path="/57067DAC/5975EFEF/5970792A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4750 4750 50  0001 C CNN
F 1 "Earth" H 4750 4850 50  0001 C CNN
F 2 "" H 4750 5000 50  0000 C CNN
F 3 "" H 4750 5000 50  0000 C CNN
	1    4750 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4950 4750 5000
Connection ~ 4750 4950
Wire Wire Line
	6950 3750 7150 3750
Wire Wire Line
	7150 3300 7150 4050
Wire Wire Line
	7150 3300 7450 3300
Wire Wire Line
	7150 4050 6950 4050
Connection ~ 7150 3750
Wire Wire Line
	6950 3850 7400 3850
Wire Wire Line
	6950 3950 7050 3950
Wire Wire Line
	7050 3950 7050 3850
Connection ~ 7050 3850
$Comp
L Earth #PWR?
U 1 1 59707F0F
P 7400 3850
AR Path="/57067DAC/5955B27F/59707F0F" Ref="#PWR?"  Part="1" 
AR Path="/57067DAC/5955AA0A/59707F0F" Ref="#PWR?"  Part="1" 
AR Path="/57067DAC/5975EFEF/59707F0F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7400 3600 50  0001 C CNN
F 1 "Earth" H 7400 3700 50  0001 C CNN
F 2 "" H 7400 3850 50  0000 C CNN
F 3 "" H 7400 3850 50  0000 C CNN
	1    7400 3850
	1    0    0    -1  
$EndComp
Text Label 7350 4050 0    60   ~ 0
GNDLow
Text Label 4600 5200 0    60   ~ 0
GNDHigh
Text Label 7350 3250 0    60   ~ 0
+5VLow
Text Label 4000 3250 0    60   ~ 0
+5VHigh
Wire Wire Line
	5650 3850 6150 3850
Wire Wire Line
	6150 3950 6100 3950
Wire Wire Line
	6100 3950 6100 3850
Connection ~ 6100 3850
Wire Wire Line
	6150 3750 6000 3750
Wire Wire Line
	6000 3300 6000 4050
Wire Wire Line
	6000 4050 6150 4050
Wire Wire Line
	5950 4050 5950 4250
Wire Wire Line
	5300 4050 5950 4050
Connection ~ 6000 3750
$Comp
L Earth #PWR?
U 1 1 597081CA
P 5650 3850
AR Path="/57067DAC/5955B27F/597081CA" Ref="#PWR?"  Part="1" 
AR Path="/57067DAC/5955AA0A/597081CA" Ref="#PWR?"  Part="1" 
AR Path="/57067DAC/5975EFEF/597081CA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5650 3600 50  0001 C CNN
F 1 "Earth" H 5650 3700 50  0001 C CNN
F 2 "" H 5650 3850 50  0000 C CNN
F 3 "" H 5650 3850 50  0000 C CNN
	1    5650 3850
	-1   0    0    1   
$EndComp
Text Label 5450 3700 0    60   ~ 0
GNDHigh
Wire Wire Line
	6950 4250 7350 4250
Wire Wire Line
	6950 4350 7350 4350
Wire Wire Line
	6950 4450 7350 4450
Wire Wire Line
	6950 4550 7350 4550
Wire Wire Line
	3800 3850 4100 3850
Wire Wire Line
	3800 3950 4100 3950
Wire Wire Line
	3800 4050 4100 4050
Wire Wire Line
	3800 4150 4100 4150
Wire Wire Line
	3800 4250 4100 4250
Wire Wire Line
	3800 4350 4100 4350
NoConn ~ 4100 4450
NoConn ~ 4100 4550
Text Label 3800 3850 0    48   ~ 0
VmiL+
Text Label 3800 3950 0    48   ~ 0
VmiH+
Text Label 3800 4050 0    48   ~ 0
VmL+
Text Label 3800 4150 0    48   ~ 0
VmH+
Text Label 3800 4250 0    48   ~ 0
VmT1+
Text Label 3800 4350 0    48   ~ 0
VmT2+
Text Label 1450 1150 2    60   ~ 0
VmiH+
Wire Wire Line
	1050 1150 1450 1150
Text HLabel 1050 1150 0    60   Input ~ 0
VmiH+
Text Label 1450 1300 2    60   ~ 0
VmL+
Wire Wire Line
	1050 1300 1450 1300
Text HLabel 1050 1300 0    60   Input ~ 0
VmL+
Text Label 1450 1450 2    60   ~ 0
VmH+
Wire Wire Line
	1050 1450 1450 1450
Text HLabel 1050 1450 0    60   Input ~ 0
VmH+
Text Label 1450 1750 2    60   ~ 0
VmT2+
Wire Wire Line
	1050 1750 1450 1750
Text HLabel 1050 1750 0    60   Input ~ 0
VmT2+
Text Label 7100 4250 0    60   ~ 0
clk
Text Label 7050 4350 0    60   ~ 0
mosi
Text Label 7100 4450 0    60   ~ 0
ss
Text Label 7050 4550 0    60   ~ 0
miso
$EndSCHEMATC
