/*
 * pwm.c
 *
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include "pwm.h"

void pwm_init(void)
{
	// Utilisation de 2 compteurs synchronisés avec une valeur max de 80
	// pour atteindre la frequence de 100KHz
	
	GTCCR = (1<<TSM) | (1<<PSRASY) | (1<<PSRSYNC);	// halt all timers
	
	// pwmH	PD3	TC2	OC2B
	// TC2 setup :
	// WGM (Waveform Generation Mode) = 101 : PWM, Phase Correct with OCRA as TOP
	// COM2A = 00 : OC2A disconnected
	// COM2B = 10 : not-inverted
	// CS = 001 : ClkIO/1 (No prescaling)
	TCCR2A = (1<<COM2B1) | (1<<WGM20);
	TCCR2B = (1<<WGM22) | (1<<CS20);
	OCR2A = PWM_CT_TOP; // set top limit
	OCR2B = 0; // output low
	TCNT2 = 0; // reset

	// pwmL	PD5	TC0	OC0B
	// TC0 setup :
	// WGM (Waveform Generation Mode) = 101 : PWM, Phase Correct with OCRA as TOP
	// COM0A = 00 : OC0A disconnected
	// COM0B = 11 : inverted
	// CS = 001 : ClkIO/1 (No prescaling)
	TCCR0A = (1<<COM0B1) | (1<<COM2B0) | (1<<WGM00);
	TCCR0B = (1<<WGM02) | (1<<CS00);
	OCR0A = PWM_CT_TOP; // set top limit
	OCR0B = PWM_CT_TOP; // output low
	TCNT0 = 0; // reset

	DDRD |= (1<<3) | (1<<5); // set outputs
	
	TIMSK0 |= (1<<OCIE0A); // enable interrupt on compare match
	
	GTCCR = 0; // release all timers
}

void pwm_stop(void)
{
	pwm_run = 0;
	OCR2B = 0;
	OCR0B = PWM_CT_TOP;
}

ISR(TIMER0_COMPA_vect)
{
	//PORTC |= 4;
	
	if(pwm_run)
	{
		OCR0B = pwm_int[pwm_idx++];
		OCR2B = OCR0B - PWM_DEAD_TIME;
		pwm_idx &= 7; // pwm index reset (8-1)
	}
	
	timer_10us = 1;

	//PORTC &= ~4;
	// execution time: 2µs
}

