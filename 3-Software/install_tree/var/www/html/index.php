<?php
/*
 * OwnWall Manager index.php
 * 
 * Web frontend for the Ownwall Manager
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
 
if($_SERVER["QUERY_STRING"])	# Asynchronous javascript request or file download
{
	if(@$_GET["mesures"])		# For mesurements
	{
		readfile("/var/local/ownwall/".$_GET["mesures"]."_mesures.json");
		exit(0);
	}
	if(@$_GET["setup"])			# Or for setup
	{
		file_put_contents("/var/local/ownwall/".$_GET["setup"]."_cmd", $_GET["var"]." ".$_GET["val"]);
		sleep(1); # let backend update setup file
		readfile("/var/local/ownwall/".$_GET["setup"]."_setup.json");
		exit(0);
	}
	if(isset($_GET["capture"]))	# Capture file download
	{							# using isset because board_id can be 0
		$captures = array();
		foreach(scandir("/var/local/ownwall") as $file)
			if(preg_match("/capture_".$_GET["capture"]."_/", $file)) array_push($captures, $file);
		$file = array_pop($captures);
		header('Content-Type: text/plain');
		header('Content-Disposition: attachment; filename="'.$file.'"');
		readfile("/var/local/ownwall/".$file);
		exit(0);
	}
}
else 							# Interface load or reload
{
	# list USB serial devices
	$serial_dev = array();
	foreach(scandir("/dev") as $device)
		if(preg_match("/ttyUSB/", $device)) array_push($serial_dev, $device);

	# read boards setups and mesures
	$board = array();
	$mesure = array();
	foreach($serial_dev as $dev)
	{
		$board[$dev] = json_decode(file_get_contents("/var/local/ownwall/$dev"."_setup.json"),true);
		$mesure[$dev] = json_decode(file_get_contents("/var/local/ownwall/$dev"."_mesures.json"),true);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
<head>
	<title>Ownwall Manager</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name='viewport' content="height=device-height, initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<style type="text/css">
		* { color: grey; background: black }
		table { border: 1px solid grey; float: left; margin: 0 10px 0 0 }
		th { text-align: left; }
		input[type="text"] { width: 60px; }
		form { float: left; }
	</style>
	<script type="text/javascript">
	window.onload=function()
	{
		// asynchronous json get function
		var get = function(url,func)
		{
			var req= new XMLHttpRequest();
			req.open("GET", url, true);
			req.send(null);
			req.onreadystatechange = function()
			{
				if(req.readyState == 4) func(req.responseText);
			}
		}
		
		// update mesures every 250 ms
		setInterval(function()
		{
<?php
	foreach($serial_dev as $dev) # One request by device
	{
?>
			get('?mesures=<?=$dev?>',function(str)
			{
				var mesures = JSON.parse(str);
				document.getElementById("<?=$dev?>_VmiL").innerHTML = mesures.VmiL+"A";
				document.getElementById("<?=$dev?>_VmiH").innerHTML = mesures.VmiH+"A";
				document.getElementById("<?=$dev?>_VmL").innerHTML = mesures.VmL+"V";
				document.getElementById("<?=$dev?>_VmH").innerHTML = mesures.VmH+"V";
				document.getElementById("<?=$dev?>_VmT1").innerHTML = mesures.VmT1+"°C";
				document.getElementById("<?=$dev?>_Pgood").innerHTML = mesures.Pgood;
				document.getElementById("<?=$dev?>_PWM").innerHTML = mesures.PWM+"%";
			});
<?php
	} // end foreach($serial_dev as $dev)
?>
		},250);
		
		// managing inputs
		var inputs = document.getElementsByTagName('input');
		for(var i=0,j=inputs.length;i<j;i++)
		{
			var input = inputs[i];
			// inputs type text
			if(input.type=="text"){
				input.onclick = function(){ this.select() };
				input.onchange = function()
				{
					var obj = this;
					get('?setup='+this.className+'&var='+this.name+'&val='+this.value,function(str)
					{
						var setup = JSON.parse(str);
						obj.value = setup[obj.name];
						obj.select();
					});
				};
			}
			// inputs type checkbox
			if(input.type=="checkbox"){
				input.onchange = function()
				{
					var obj = this;
					if(this.checked) var value = 1;
					else var value = 0;
					get('?setup='+this.className+'&var='+this.name+'&val='+value,function(str)
					{
						var setup = JSON.parse(str);
						obj.checked = setup[obj.name];
					});
				};			
			}
		}
		// managing selects
		var selects = document.getElementsByTagName('select');
		for(var i=0,j=selects.length;i<j;i++)
		{
			var select = selects[i];
			select.onchange = function()
			{
				var obj = this;
				get('?setup='+this.className+'&var='+this.name+'&val='+this.value,function(str)
				{
					var setup = JSON.parse(str);
					obj.value = setup[obj.name];
				});
			};
		}
	};
	</script>       
</head>
<body>
<?php
if(count($board))
{
	foreach($board as $dev=>$setup){
?>
	<form name="<?=time()/* force browser to update values */?>">
		<fieldset>
			<legend><?=$dev?></legend>
			<table>
				<tr><th>Board ID</th><td>
					<input type="text" class="<?=$dev?>" name="board_id" value="<?=$setup["board_id"]?>" />
				</td></tr>
				<tr><th>Duty-cycle</th><td>
					<input type="text" class="<?=$dev?>" name="duty_cycle" value="<?=$setup["duty_cycle"]?>" />
				</td></tr>
				<tr><th>Tracking channel</th><td>
					<select class="<?=$dev?>" name="mesure_cha">
						<option value="0"<?=$setup["mesure_cha"]==0 ? " selected":""?>>VmiL</option>
						<option value="1"<?=$setup["mesure_cha"]==1 ? " selected":""?>>VmiH</option>
						<option value="2"<?=$setup["mesure_cha"]==2 ? " selected":""?>>VmL</option>
						<option value="3"<?=$setup["mesure_cha"]==3 ? " selected":""?>>VmH</option>
					</select>
				</td></tr>
				<tr><th>Tracked value</th><td>
					<input type="text" class="<?=$dev?>" name="setting" value="<?=$setup["setting"]?>" />
				</td></tr>
				<tr><th>Tracking</th><td>
					<input type="checkbox" class="<?=$dev?>" name="tracking_on"<?=$setup["tracking_on"] ? " checked":""?> />
				</td></tr>
				<tr><th>PWM running</th><td>
					<input type="checkbox" class="<?=$dev?>" name="pwm_run"<?=$setup["pwm_run"] ? " checked":""?> />
				</td></tr>
			</table>
			<table style="width:150px">
				<tr>
					<th>Variable</th>
					<th>Caliber</th>
					<th>Offset</th>
					<th>Value</th>
				</tr>
				<tr>
					<th>VmiL</th>
					<td><input type="text" class="<?=$dev?>" name="vmil_cal" value="<?=$setup["vmil_cal"]?>" /></td>
					<td><input type="text" class="<?=$dev?>" name="vmil_offset" value="<?=$setup["vmil_offset"]?>" /></td>
					<td id="<?=$dev?>_VmiL"><?=$mesure[$dev]["VmiL"]?>A</td>
				</tr>
				<tr>
					<th>VmiH</th>
					<td><input type="text" class="<?=$dev?>" name="vmih_cal" value="<?=$setup["vmih_cal"]?>" /></td>
					<td><input type="text" class="<?=$dev?>" name="vmih_offset" value="<?=$setup["vmih_offset"]?>" /></td>
					<td id="<?=$dev?>_VmiH"><?=$mesure[$dev]["VmiH"]?>A</td>
				</tr>
				<tr>
					<th>VmL</th>
					<td><input type="text" class="<?=$dev?>" name="vml_cal" value="<?=$setup["vml_cal"]?>" /></td>
					<td>&nbsp;</td>
					<td id="<?=$dev?>_VmL"><?=$mesure[$dev]["VmL"]?>V</td>
				</tr>
				<tr>
					<th>VmH</th>
					<td><input type="text" class="<?=$dev?>" name="vmh_cal" value="<?=$setup["vmh_cal"]?>" /></td>
					<td>&nbsp;</td>
					<td id="<?=$dev?>_VmH"><?=$mesure[$dev]["VmH"]?>V</td>
				</tr>
				<tr>
					<th>VmT1</th>
					<td><input type="text" class="<?=$dev?>" name="vmt1_cal" value="<?=$setup["vmt1_cal"]?>" /></td>
					<td>&nbsp;</td>
					<td id="<?=$dev?>_VmT1"><?=$mesure[$dev]["VmT1"]?>°C</td>
				</tr>
				<tr>
					<th>Pgood</th>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td id="<?=$dev?>_Pgood"><?=$mesure[$dev]["Pgood"]?></td>
				</tr>
				<tr>
					<th>PWM</th>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td id="<?=$dev?>_PWM"><?=$mesure[$dev]["PWM"]?>%</td>
				</tr>
			</table>
		</fieldset>
	</form>
<?php
		} // end foreach($board as $dev=>$setup)
	} // end of if(count($board))
	else
	{
?>
	No ttyUSB device found !
<?php		
	} // end else of if(count($board))
} // end else of if($_SERVER["QUERY_STRING"])
?>
</body>
</html>

