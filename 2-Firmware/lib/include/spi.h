/*
 * spi.h
 *
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint8_t spi_buffer[3];

void spi_init(void);			// SPI initialisation (mode 1, 8MHz

#define SPI_START\
	PORTB &= ~(1<<2); 		/* !SS low */\
	SPDR = spi_buffer[1];	/* writing the first command byte */

#define SPI_2\
	SPDR = spi_buffer[2];	/* writing the second command byte */

#define SPI_3\
	spi_buffer[1] = SPDR;	/* storing the first response byte from SPI */\
	SPDR = 0;				/* sending a NULL command byte */

#define SPI_END\
	spi_buffer[2] = SPDR;	/* storing the last response byte from SPI */\
	PORTB |= (1<<2); 		/* !SS high */




#endif /* SPI_H_ */
